#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from RDD device
$(call inherit-product, device/hmd/RDD/device.mk)

PRODUCT_DEVICE := RDD
PRODUCT_NAME := lineage_RDD
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia T20
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd-rvo3

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="Riddler_user_12_SP1A.210812.016_00WW_2_380_release-keys"

BUILD_FINGERPRINT := Nokia/Riddler_00WW/RDD:12/SP1A.210812.016/00WW_2_380:user/release-keys
