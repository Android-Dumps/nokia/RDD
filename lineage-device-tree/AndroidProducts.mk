#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_RDD.mk

COMMON_LUNCH_CHOICES := \
    lineage_RDD-user \
    lineage_RDD-userdebug \
    lineage_RDD-eng
