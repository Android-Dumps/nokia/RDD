#!/vendor/bin/sh

LOG_TAG="sar_wifi.sh"

function exe_log()
{
	local cmd="$@"
	local emsg="$(eval $cmd 2>&1)"

	log -p d -t "$LOG_TAG" "$cmd ecode=$? $emsg"
}

function gro_switch()
{
	exe_log "echo $1 > /sys/module/seth/parameters/gro_enable"
}

function sar_wifi_on() {
    setprop persist.vendor.sar_wifi.done false
    # 2G All
	exe_log "iwnpi wlan0 set_sar_power 2 4 7"
    /vendor/bin/iwnpi wlan0 set_sar_power 2 4 7
    # 5G 11A
	exe_log "iwnpi wlan0 set_sar_power 2 7 4"
    /vendor/bin/iwnpi wlan0 set_sar_power 2 7 4
    # 5G 11N
	exe_log "iwnpi wlan0 set_sar_power 2 7 5"
    /vendor/bin/iwnpi wlan0 set_sar_power 2 7 5
    # 5G 11AC
	exe_log "iwnpi wlan0 set_sar_power 2 7 6"
    /vendor/bin/iwnpi wlan0 set_sar_power 2 7 6

    setprop persist.vendor.sar_wifi.done true
}

function sar_wifi_off() {
    setprop persist.vendor.sar_wifi.done false
	exe_log "iwnpi wlan0 set_sar_power 0 127 7"
    /vendor/bin/iwnpi wlan0 set_sar_power 0 127 7
    setprop persist.vendor.sar_wifi.done true
}



if [ "$1" = "on" ]; then
	sar_wifi_on;
elif [ "$1" = "off" ]; then
	sar_wifi_off;
fi
